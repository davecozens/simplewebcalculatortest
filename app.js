var Calculator = React.createClass({displayName: "Calculator",
    getInitialState: function() {
        return {
            currentTotal: "0",
            buffer:"0",
            currentDisplay:"0",
            lastKey:""
        };
    },
    handleNumber: function(value){
        var tmpBuffer=this.state.buffer;
        if(this.state.lastKey=="="){
          this.setState({currentTotal: 0});          
        }
        if(tmpBuffer==0){
          tmpBuffer=value;
        } else {
          tmpBuffer=tmpBuffer+value;
        }
        this.setState({buffer: tmpBuffer});
        this.setState({currentDisplay: tmpBuffer});
        this.setState({lastKey: value});
    },
    handlePlus: function(){
      var currentTotal=eval(this.state.currentTotal);
      var currentBuffer=eval(this.state.buffer);
      var newTotal=eval(currentTotal+currentBuffer);
      this.setState({currentTotal: newTotal});
      this.setState({buffer: 0});
      this.setState({currentDisplay: newTotal});
      this.setState({lastKey: "+"});
    },
    handleEquals: function(){
      var currentTotal=eval(this.state.currentTotal);
      var currentBuffer=eval(this.state.buffer);
      var newTotal=eval(currentTotal+currentBuffer);
      this.setState({currentTotal: newTotal});
      this.setState({buffer: 0});
      this.setState({currentDisplay: newTotal});
      this.setState({lastKey: "="});
    },
    handleReset: function(){
      this.setState({currentTotal: 0});
      this.setState({buffer: 0});
      this.setState({currentDisplay: 0});
    },
    handleClearBuffer: function(){
      this.setState({buffer: 0});
      this.setState({currentDisplay: 0});
    },

  	render: function() {
       var displayValue={
            value: this.state.currentDisplay,
            requestChange: this.update
        };
    	return (
	      React.createElement("div", {className: "calculator"}, 
	        React.createElement(Display, {buffer: this.state.currentDisplay}), 
	        React.createElement(Button, {type: "button n", onClick: this.handleNumber, label: "1"}), 
	        React.createElement(Button, {type: "button n", onClick: this.handleNumber, label: "2"}), 
	        React.createElement(Button, {type: "button n", onClick: this.handleNumber, label: "3"}), 
	        React.createElement(Button, {type: "button n", onClick: this.handleNumber, label: "4"}), 
	        React.createElement(Button, {type: "button n", onClick: this.handleNumber, label: "5"}), 
	        React.createElement(Button, {type: "button n", onClick: this.handleNumber, label: "6"}), 
	        React.createElement(Button, {type: "button n", onClick: this.handleNumber, label: "7"}), 
	        React.createElement(Button, {type: "button n", onClick: this.handleNumber, label: "8"}), 
	        React.createElement(Button, {type: "button n", onClick: this.handleNumber, label: "9"}), 
	        React.createElement(Button, {type: "button c", onClick: this.handleClearBuffer, label: "CE"}), 
	        React.createElement(Button, {type: "button n", onClick: this.handleNumber, label: "0"}), 
	        React.createElement(Button, {type: "button c", onClick: this.handleReset, label: "C"}), 
	        React.createElement(Button, {type: "button f", onClick: this.handlePlus, label: "+"}), 
	        React.createElement(Button, {type: "button f", onClick: this.handleEquals, label: "="})
	      )
    	);
  	}
});

var Display = React.createClass({displayName: "Display",
  render: function() {
    return (
      React.createElement("div", {className: "display"}, React.createElement("div", null, 
        this.props.buffer
      ))
    );
  }
});

var Button = React.createClass({displayName: "Button",
  onClick: function(){
    this.props.onClick(this.props.label);
  },
  render: function() {
    return (
      React.createElement("div", {className: "button", onClick: this.onClick, className: this.props.type}, React.createElement("div", null, 
        this.props.label
      ))
    );
  }
});

React.render(
  React.createElement(Calculator, null),
  document.getElementById('content')
);